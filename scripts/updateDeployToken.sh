#!/bin/bash
#  Original Commands
#  - sed -i 's/#CommonLibDeployTokenValue#/'$CommonLibDeployToken2'/g' common-entities/gradle.properties

# Update Deploy Token based on ENV parameters
set -e
echo "Update Deploy Token Start"
#pwd
#ls -al
sed -i 's/#CommonLibDeployTokenValue#/'$CommonLibDeployToken'/g' build.gradle
cat -n build.gradle | grep gitLabDeployToken 
echo -e "\nUpdate Deploy Token End"
